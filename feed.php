<?php

$numberOfRows = $_GET['host'];
$file = '';

if (is_numeric($numberOfRows)) {

    switch($numberOfRows) {
        case 10000:
            $file = 'feed_long.json';
            break;
        case 2:
        default:
            $file = 'feed.json';
            break;
    }

    header('Content-Type: application/json');
    readfile($file);

}else{
    // return a basic error code if no proper host URL param
    http_response_code(400);
}
