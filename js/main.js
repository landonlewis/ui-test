(function($) {
    var self = this,

        // Adjust this to return the number of hosts
        // Possible values are 2 or 10000
        returnHosts = 2,

        requestURL = '/download/request?host=2',

        hosts = document.getElementById('hosts').getElementsByTagName('tbody')[0],
        hostFrag = document.createDocumentFragment(),

        request = function() {
            // Make the request
            $.ajax({
                data: {
                    host: returnHosts
                },
                url: requestURL,
                dataType: 'json',
                success: process,
                error: fail
            });
        },

        // Process returned JSON
        process = function(data) {
            data.configurations.forEach(function(row, index) {
                var tr = document.createElement('tr');

                // Add count as first column
                tr.appendChild(buildColumn(index + 1));

                // Loop through each column
                for (var col in row) {
                    if(row.hasOwnProperty(col) ) {
                        tr.appendChild(buildColumn(col));        
                    }
                }

                hostFrag.appendChild(tr); 
            }); 

            window.requestAnimationFrame(appendHosts);
        },

        appendHosts = function() {
            hosts.appendChild(hostFrag)
        },

        buildColumn = function(column) {
            var td = document.createElement('td');
            td.textContent = column;
            return td;
        },

        fail = function() {
            var tr = document.createElement('tr'),
                td = $(document.createElement('td')).attr('colspan', 5).addClass('error')[0];

            td.textContent = 'There was a problem processing your request!';

            tr.appendChild(td);
            hostFrag.appendChild(tr);
            hosts.appendChild(hostFrag);
        };

    $(request);
})($);
